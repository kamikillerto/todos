var console = console || window.console;
console.time('Page load in');
function createItem(object) {
	"use strict";
	var li = document.createElement('li');
	li.innerHTML = '<aside class="pack-end">'
					+	'<img class="item_menu" src="icons/menu2-20.png"/>'
					+	'<div class="popup-container popup">'
					+		'<ul>'
					+			'<li class="menuItem"><a href="views/add.html?id=' + object.id + '">Editer</a></li>'
					+			'<li class="menuItem"><a>Supprimer</a></li>'
					+		'</ul>'
					+	'</div>'
					+ '</aside>'
					+ '<h2>' + object.title + '</h2>'
					+ '<p>' + object.date + '</p>'
					+ '<p>' + object.description + '</p>';
	return li;
}
function populateList(tasks) {
	"use strict";
	var list = document.getElementById("todo-list"),
		i = 0;
	localStorage.clear();
	tasks.forEach(function (item) {
		localStorage[i++] = JSON.stringify(item);
		list.appendChild(createItem(item));
	});

}
function handleRequest(xhr) {
	"use strict";
	if (xhr.readyState === xhr.DONE && xhr.status === 200) {
		var json = JSON.parse(xhr.responseText);
		populateList(json);
	}
}
window.onload = function () {
	"use strict";
	console.timeEnd('Page load in');
	var xhr,
		tasks = [],
		i;
	if (localStorage.length === 0) {
		if (window.XMLHttpRequest) {
			xhr = new XMLHttpRequest();
			xhr.overrideMimeType('application/json');
			xhr.onreadystatechange = function () {
				handleRequest(xhr);
			};
			xhr.open('GET', 'data/todo.json', true);
			xhr.send(null);
		} else {
			console.error("Error with XMLHttpRequest");
		}
	} else {
		for (i = 0; i < localStorage.length; i++) {
			if (!isNaN(localStorage.key(i))) {
				tasks.push(JSON.parse(localStorage.getItem(i)));
			}
		}
		populateList(tasks);
	}
};

