var console = console || window.console,
	form,
	taskTitle,
	taskDate,
	taskTime,
	taskDescription,
	cancel,
	id;
console.time('Page loaded in');

function Task(title, date, time, description) {
	"use strict";
	this.title = title;
	this.date = date + " " + time;
	this.description = description;
}

function save() {
	"use strict";
	var task = new Task(taskTitle.value, taskDate.value, taskTime.value, taskDescription.value);
	if (id) {
		localStorage[id - 1] = JSON.stringify(task);
	} else {
		localStorage[localStorage.length] = JSON.stringify(task);
	}
}

function goBack() {
	"use strict";
	save();
	window.location = "../index.html";
}

function formSubmit(event) {
	"use strict";
	event.preventDefault();
	var error,
		no_error = true,
		errors = document.querySelectorAll("p.error"),
		i;
	for (i = 0; i < errors.length; i++) {
		errors[i].remove();
	}
	if (taskTitle.validity.valueMissing) {
		taskTitle.setAttribute('data-error', 'Veuillez remplir ce champ');
		error = document.createElement('p');
		error.className = "error";
		error.innerHTML = 'Veuillez remplir ce champ';
		taskTitle.insertAdjacentHTML('afterEnd', error.outerHTML);
		no_error = false;
	} else {
		taskTitle.setAttribute("data-error", "");
	}
	if (taskDate.validity.valueMissing || taskTime.validity.valueMissing) {
		taskDate.parentElement.setAttribute('data-error', 'Veuillez remplir ce champ');
		error = document.createElement('p');
		error.className = "error";
		error.innerHTML = 'Veuillez remplir ce champ';
		taskDate.parentElement.insertAdjacentHTML('afterEnd', error.outerHTML);
		no_error = false;
	} else {
		taskDate.parentElement.setAttribute("data-error", "");
	}
	if (no_error) {
		goBack();
	}
}

function cancelSubmit(event) {
	"use strict";
	event.preventDefault();
	if (taskTitle.value.trim() === '') {
		goBack();
	} else {
		console.log("Form not empty");
	}
}

window.onload = function () {
	"use strict";
	console.timeEnd('Page loaded in');
	id = window.location.search.split('=')[1];
	form = document.querySelector('form');
	taskTitle = document.getElementById('task_title');
	taskDate = document.getElementById('task_date');
	taskTime = document.getElementById('task_time');
	taskDescription = document.getElementById('task_description');

	cancel = document.getElementById('cancel');
	cancel.onclick = cancelSubmit;
	form.onsubmit = formSubmit;

	if (id) {
		console.log(localStorage[id - 1]);
		var task = JSON.parse(localStorage[id - 1]);
		taskTitle.value = task.title;
		taskDate.value = task.date.split(" ")[0];
		taskTime.value = task.date.split(" ")[1];
		taskDescription.value = task.description;
	}
};
