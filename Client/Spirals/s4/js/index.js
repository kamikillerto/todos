var console = console || window.console;
console.time('Page load in');

function removeAlarm(id) {
	"use strict";
	console.log(id);
	var task = JSON.parse(localStorage.getItem(id - 1));
	if (task && task.alarmId) {
		navigator.mozAlarms.remove(task.alarmId);
	}
	localStorage.removeItem(id - 1);
	return;
}

function createItem(object) {
	"use strict";
	console.log(object);
	var li = document.createElement('li');
	li.innerHTML = '<aside class="pack-end">'
					+	'<img class="item_menu" src="icons/menu2-20.png"/>'
					+	'<div class="popup-container popup">'
					+		'<ul>'
					+			'<li class="menuItem"><a href="views/add.html?id=' + object.id + '">Editer</a></li>'
					+			'<li class="menuItem"><a href="?remove=' + object.id + '">Supprimer</a></li>' // @TODO try to find a way to use onclick event
					+		'</ul>'
					+	'</div>'
					+ '</aside>'
					+ '<h2>' + object.title + '</h2>'
					+ '<p>' + object.date + '</p>'
					+ '<p>' + object.description + '</p>';
	return li;
}
function populateList(tasks) {
	"use strict";
	var list = document.getElementById("todo-list"),
		i = 0;
	localStorage.clear();
	console.log(localStorage);
	console.log(tasks);
	tasks.forEach(function (item) {
		item.id = i + 1;
		localStorage[i++] = JSON.stringify(item);
		list.appendChild(createItem(item));
	});

}
function handleRequest(xhr) {
	"use strict";
	if (xhr.readyState === xhr.DONE && xhr.status === 200) {
		var json = JSON.parse(xhr.responseText);
		populateList(json);
	}
}
window.onload = function () {
	"use strict";
	console.timeEnd('Page load in');
	console.log(window.location.search);
	if (window.location.search.split('=')[0] === "?remove") {
		var a = removeAlarm(window.location.search.split('=')[1]);
	}
	var xhr,
		tasks = [],
		i;
	if (localStorage.length === 0) {
		if (window.XMLHttpRequest) {
			xhr = new XMLHttpRequest();
			xhr.overrideMimeType('application/json');
			xhr.onreadystatechange = function () {
				handleRequest(xhr);
			};
			xhr.open('GET', 'data/todo.json', true);
			xhr.send(null);
		} else {
			console.error("Error with XMLHttpRequest");
		}
	} else {
		console.log(localStorage);
		for (i = 0; i < localStorage.length; i++) {
			console.log(i);
			if (!isNaN(localStorage.key(i))) {
				console.log(JSON.parse(localStorage.getItem(localStorage.key(i))));
				tasks.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
			}
		}
		populateList(tasks);
	}
};
