var console = console || window.console;
console.time('Page load in');
var items = [
	{
		id: 1,
		title: "Task one",
		date: "21/08/2015 12:00",
		description: "Task one description"
	}, {
		id: 2,
		title: "Task two",
		date: "21/08/2015 12:00",
		description: "Task two description"
	}, {
		id: 3,
		title: "Task tree",
		date: "21/08/2015 12:00",
		description: "Task tree description"
	}, {
		id: 4,
		title: "Task four",
		date: "21/08/2015 12:00",
		description: "Task four description"
	}
];
function getItem(object) {
	"use strict";
	var li = document.createElement('li');
	li.innerHTML = '<aside class="pack-end">'
					+	'<img class="item_menu" src="icons/menu2-20.png"/>'
					+	'<div class="popup-container popup">'
					+		'<ul>'
					+			'<li class="menuItem"><a href="views/add.html?id=' + object.id + '">Editer</a></li>'
					+			'<li class="menuItem"><a>Supprimer</a></li>'
					+		'</ul>'
					+	'</div>'
					+ '</aside>'
					+ '<h2>' + object.title + '</h2>'
					+ '<p>' + object.date + '</p>'
					+ '<p>' + object.description + '</p>';
	return li;

}
function populateList() {
	"use strict";
	var list = document.getElementById("todo-list");
	items.forEach(function (item) {
		list.appendChild(getItem(item));
	});
}
window.onload = function () {
	"use strict";
	console.timeEnd('Page load in');
	populateList();
};
