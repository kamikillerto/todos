var console = console || window.console,
	form,
	taskTitle,
	taskDate,
	taskTime,
	cancel,
	id;
console.time('Page loaded in');

function goBack() {
	"use strict";
	window.history.back();
}

function formSubmit(event) {
	"use strict";
	event.preventDefault();
	var error,
		no_error,
		errors = document.querySelectorAll("p.error"),
		i;
	for (i = 0; i < errors.length; i++) {
		errors[i].remove();
	}
	if (taskTitle.validity.valueMissing) {
		taskTitle.setAttribute('data-error', 'Veuillez remplir ce champ');
		error = document.createElement('p');
		error.className = "error";
		error.innerHTML = 'Veuillez remplir ce champ';
		taskTitle.insertAdjacentHTML('afterEnd', error.outerHTML);
		no_error = false;
	} else {
		taskTitle.setAttribute("data-error", "");
	}
	if (taskDate.validity.valueMissing || taskTime.validity.valueMissing) {
		taskDate.parentElement.setAttribute('data-error', 'Veuillez remplir ce champ');
		error = document.createElement('p');
		error.className = "error";
		error.innerHTML = 'Veuillez remplir ce champ';
		taskDate.parentElement.insertAdjacentHTML('afterEnd', error.outerHTML);
		no_error = false;
	} else {
		taskDate.parentElement.setAttribute("data-error", "");
	}
	if (no_error) {
		goBack();
	}
}

function cancelSubmit(event) {
	"use strict";
	event.preventDefault();
	if (taskTitle.value.trim() === '') {
		goBack();
	} else {
		console.log("Form not empty");
	}
}

window.onload = function () {
	"use strict";
	console.timeEnd('Page loaded in');
	id = window.location.search.split('=')[1];
	form = document.querySelector('form');
	taskTitle = document.getElementById('task_title');
	taskDate = document.getElementById('task_date');

	taskDate.setAttribute('data-error', 'Veuillez remplir ce champ');

	taskTime = document.getElementById('task_time');
	cancel = document.getElementById('cancel');
	cancel.onclick = cancelSubmit;
	form.onsubmit = formSubmit;
};
