require.config({
    baseUrl: './js/',
    paths: {
        require: '../bower_components/alameda/alameda',
        text: '../bower_components/requirejs-text/text',
        app: 'app',
        lib: 'lib',
        controller: 'app/controller',
        view: 'app/view',
        model: 'app/model'
    }
});
