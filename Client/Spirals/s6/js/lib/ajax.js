define(function () {
    "use strict";
    return (function () {
        var ajax = {};
        ajax.init = function (events) {
            var xhr = new XMLHttpRequest(),
                event;
            xhr.withCredentials = true; // to remove?
            for (event in events) {
                xhr.addEventListener(event, events[event]);
            }
            return xhr;
        };
        ajax.post = function (url, settings) {
            var xhr = this.init(settings.events);
            xhr.open('POST', url, true);
            if (settings.contentType && settings.contentType !== '') {
                xhr.setRequestHeader("Content-Type", settings.contentType);
            }
            xhr.send(settings.data);
        };
        ajax.get = function (url, settings) {
            var xhr = this.init(settings.events);
            xhr.open('GET', url, true);
            if (settings.contentType && settings.contentType !== '') {
                xhr.setRequestHeader("Content-Type", settings.contentType);
            }
            xhr.send(settings.data);
        };
        return function (method, url, settings) {
            ajax[method](url, settings);
        };

    }());
});

/*
  onabort: null
  onerror: null
  onload: null
  onloadend: null
  onloadstart: null
  onprogress: null
  onreadystatechange: null
  ontimeout: null
*/
/*
	xhr.addEventListener("readystatechange", function () {
		if (this.readyState === this.DONE && (this.status === 201 || this.status === 200)) {
		}
	});
*/
/*
	xhr.open("POST", localStorage.getItem('baseUrl'), true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(data);
*/
