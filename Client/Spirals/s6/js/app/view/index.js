define(['./base', 'text!../../../templates/index.html'], function (Base, view) {
    "use strict";
    var indexView = new Base(view);
    indexView.list = indexView.template.querySelector("#todo-list");
    indexView.append = [];

    indexView.createTask = function (task) {
        var li = document.createElement('li');
        li.innerHTML = '<aside class="pack-end">' +
            '<img class="item_menu" src="icons/menu2-20.png"/>' +
            '<div class="popup-container popup">' +
            '<ul>' +
            '<li class="menuItem"><a data-id="' + task.id + '">Editer</a></li>' +
            '<li class="menuItem"><a href="?remove=' + task.id + '">Supprimer</a></li>' +
            '</ul>' +
            '</div>' +
            '</aside>' +
            '<h2>' + task.title + '</h2>' +
            '<p>' + task.date + '</p>' +
            '<p>' + task.description + '</p>';
        return li;
    };
    indexView.appendTasks = function (tasks, callback) {
        var i = 0;
        tasks.forEach(function (item) {
            indexView.append.push(indexView.createTask(item));
        });
        this.append.forEach(function (el) {
            indexView.list.appendChild(el);
        });
        if (typeof callback === 'function') {
            callback();
        }
    };
    indexView.render = function (tasks, callback) {
        this.append.forEach(function (el) {
            el.remove();
        });
        this.append = [];
        document.body = this.template;
    };
    return indexView;
});
