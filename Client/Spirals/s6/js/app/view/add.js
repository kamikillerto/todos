define(['./base', 'text!../../../templates/add.html'], function (Base, template) {
    "use strict";
    var view = new Base(template);
    view.form = view.querySelector('form');
    view.taskTitle = view.querySelector('#task_title');
    view.taskDate = view.querySelector('#task_date');
    view.taskTime = view.querySelector('#task_time');
    view.taskDescription = view.querySelector('#task_description');
    view.validForm = function () {

    };
    view.render = function (task, error) {
        this.taskTitle.value = '';
        this.taskDate.value = '';
        this.taskTime.value = '';
        this.taskDescription.value = '';
        if (task) {
            this.taskTitle.value = task.title;
            this.taskDate.value = task.date.split("T")[0];
            this.taskTime.value = task.date.split("T")[1];
            this.taskDescription = task.description;
        }
        document.body = this.template;
    };
    return view;
});
