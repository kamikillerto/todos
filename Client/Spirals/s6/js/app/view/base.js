define(['lib/converter'], function (converter) {
    "use strict";

    function ViewBase(template) {
        this.template = converter(template);
    }
    ViewBase.prototype = {
        render: function () {
            document.body = this.template;
        },
        querySelector: function (selector) {
            return this.template.querySelector(selector);
        },
        querySelectorAll: function (selector) {
            return this.template.querySelectorAll(selector);
        }
    };

    return ViewBase;
});
