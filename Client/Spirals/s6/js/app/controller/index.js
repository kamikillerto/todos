define(['./base', 'view/index', 'lib/ajax', 'lib/HTTP', 'model/task'], function (Base, index, ajax, HTTP, Task) {
    "use strict";
    var controller = new Base("Index controller");
    controller.view = index;
    controller.tasks = new Map();
    controller.appendTasks = function () {
        this.view.appendTasks(this.tasks, function () {
            controller.on('click', 'section .menuItem:first-child > a', function () {
                controller.uber.setContext('add', controller.tasks.get(this.dataset.id));
            });
        });
    };
    controller.init = function () {
        this.on('click', '#add', function (event) {
            controller.uber.setContext('add');
        });
        ajax('get',
            'http://todos.kermit.orange-labs.fr/TodosRest/rest', {
                contentType: "application/json",
                events: {
                    error: function () {
                        console.log('error');
                    },
                    load: function () {
                        Task.fromJSON(this.response).forEach(function (task) {
                            controller.tasks.set(task.id.toString(), task);
                        });
                        controller.appendTasks();
                    }
                }
            }
            );
    };
    controller.render = function (task) {
        if (task instanceof Task) {
            this.tasks.set(task.id.toString(), task);
        }
        this.view.render();
        this.appendTasks();
    };
    controller.removeAlarm = function () {

    };
    return controller;
});
