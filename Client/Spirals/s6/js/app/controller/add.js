define(['./base', 'view/add', 'model/task', 'lib/ajax', 'lib/HTTP'], function (Base, add, Task, ajax, HTTP) {
    "use strict";
    var controller = new Base("Add controller");
    controller.view = add;
    controller.init = function () {
        var taskTitle = this.view.querySelector('#task_title'),
            taskDate = this.view.querySelector('#task_date'),
            taskTime = this.view.querySelector('#task_time'),
            taskDescription = this.view.querySelector('#task_description');

        this.on('submit', 'form', function (event) {
            event.preventDefault();

        });
        this.on('click', '#cancel', function (event) {
            if (taskTitle.value.trim() === '') {
                controller.uber.setContext('index');
            } else {
                console.log("Form not empty");
            }
        });
    };
    return controller;
});
