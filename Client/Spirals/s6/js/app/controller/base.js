define(function () {
    "use strict";

    function ControllerBase(id) {
        this.id = id;
    }

    ControllerBase.prototype = {
        get view() {
            return this._view;
        },
        set view(value) {
            this._view = value;
        },
        get uber() {
            return this._uber;
        },
        set uber(value) {
            this._uber = value;
        },
        render: function (f) {
            this.view.render(f);
        },
        on: function (event, selector, callback) {
            if (/#/.exec(selector) !== null) { // should be only first letter
                this.view.querySelector(selector).addEventListener(event, callback);
            } else {
                var els = this.view.querySelectorAll(selector),
                    el;
                for (el in els) {
                    if (els.hasOwnProperty(el)) {
                        els[el]['on' + event] = callback;
                    }
                }
            }
        }
    };

    return ControllerBase;
});
