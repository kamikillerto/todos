define(function () {
    "use strict";

    function Task(id, title, description, date, time) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.time = time;
    }
    Task.fromJSON = function (json) {
        json = JSON.parse(json);
        if (Array.isArray(json)) {
            var tasks = [];
            json.forEach(function (task) {
                tasks.push(new Task(task.idTodo, task.title, task.description, task.date, task.time));
            });
            return tasks;
        } else {
            return new Task(json.idTodo, json.title, json.description, json.date, json.time);
        }
    };
    return Task;
});
