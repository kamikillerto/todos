var console = console || window.console;
console.time('Page load in');
localStorage.setItem("baseUrl", "http://todos.kermit.orange-labs.fr/TodosRest/rest");

function handleDeleteRequest(xhr) {
	"use strict";
	console.log(xhr);
	if (xhr.readyState === xhr.DONE && xhr.status === 200) {
		var json = JSON.parse(xhr.responseText);
		console.log(json);
	}
}

function removeAlarm(id) {
	"use strict";
	var xhr;
	if (window.XMLHttpRequest) {

		var data = localStorage.getItem(id);

		xhr = new XMLHttpRequest();
		xhr.withCredentials = true;

		xhr.addEventListener("readystatechange", function () {
			if (this.readyState === this.DONE && this.status === 200) {
				getTasks();
			}
		});

		xhr.open("DELETE", "http://todos.kermit.orange-labs.fr/TodosRest/rest");
		xhr.setRequestHeader("Content-Type", "application/json");

		xhr.send(data);
	} else {
		console.error("Error with XMLHttpRequest");
	}
	return;
}

function createItem(object) {
	"use strict";
	console.log(object);
	var li = document.createElement('li');
	li.innerHTML = '<aside class="pack-end">' + '<img class="item_menu" src="icons/menu2-20.png"/>' + '<div class="popup-container popup">' + '<ul>' + '<li class="menuItem"><a href="views/add.html?id=' + object.idTodo + '">Editer</a></li>' + '<li class="menuItem"><a href="?remove=' + object.idTodo + '">Supprimer</a></li>' // @TODO try to find a way to use onclick event
		+ '</ul>' + '</div>' + '</aside>' + '<h2>' + object.title + '</h2>' + '<p>' + object.date + '</p>' + '<p>' + object.description + '</p>';
	return li;
}

function populateList(tasks) {
	"use strict";
	var list = document.getElementById("todo-list"),
		i = 0;
	tasks.forEach(function (item) {
		list.appendChild(createItem(item));
		localStorage.setItem(item.idTodo, JSON.stringify(item));
	});

}

function handleRequest(xhr) {
	"use strict";
	if (xhr.readyState === xhr.DONE && xhr.status === 200) {
		var json = JSON.parse(xhr.responseText);
		populateList(json);
	}
}

function getTasks() {
	var xhr;
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
		xhr.overrideMimeType('application/json');
		xhr.addEventListener('readystatechange', handleRequest);
		xhr.open('GET', localStorage.getItem('baseUrl'), true);
		xhr.send(null);
	} else {
		console.error("Error with XMLHttpRequest");
	}
}
window.onload = function () {
	"use strict";
	console.timeEnd('Page load in');
	if (window.location.search.split('=')[0] === "?remove") {
		var a = removeAlarm(window.location.search.split('=')[1]);
	} else {
		getTasks();
	}
};
